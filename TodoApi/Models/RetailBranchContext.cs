using Microsoft.EntityFrameworkCore;

namespace TodoApi.Models;

public class TodoContext: DbContext{
    public TodoContext(DbContextOptions<TodoContext> options):  base(options){
    }
    public DbSet<RetailBranch> RetailBranches { get; set; } = null!;

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<RetailBranch>().HasKey(x => x.BranchId);
        base.OnModelCreating(modelBuilder);
    }
}