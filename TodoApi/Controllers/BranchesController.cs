using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Text.Encodings.Web;
using TodoApi.Models;

[Route("api/BranchController")]
[ApiController]
public class BranchController : Controller{
    public string constructor{
        get{
            return "";
        }
        set{}
    }
    public string add{
        get{
            return "";
        }
        set{}
    }
    public string edit{
        get{
            return "";
        }
        set{}
    }
    public string details{
        get{
            return "";
        }
        set{}
    }
    public string get{
        get{
            return "";
        }
    }
    public string remove{
        set{
            return;
        }
    }
    // app.MapGet("/todoitems", async (TodoContext db) =>
    //     await db.RetailBranches.ToListAsync()
    // )

    [HttpGet]
    public async Task<List<RetailBranch>> Get(TodoContext db){
        return await db.RetailBranches.ToListAsync();
    }

    // app.MapPost("/todoitems", async (RetailBranch todo, TodoContext db) =>
    //     {
    //         db.RetailBranches.Add(todo);
    //         await db.SaveChangesAsync();
    //         return Results.Created($"/insert directory of db/{todo.BranchId}", todo);
    //     }
    // );

    [HttpPost]
    public async Task<ActionResult<RetailBranch>> Post(RetailBranch todo, TodoContext db){
        db.RetailBranches.Add(todo);
        
        await db.SaveChangesAsync();

        return CreatedAtAction(nameof(Get), new {id = todo.BranchId}, todo);
    }

    // app.MapPut("/todoitems/{id}", async (int id, RetailBranch input, TodoContext db) => {
    //     var todo = await db.RetailBranches.FindAsync(id);

    //     if (todo is null) return Results.NotFound();

    //     todo.Name = input.Name;
    //     // set state of item in database
    //     await db.SaveChangesAsync();

    //     return Results.NoContent();
    // });

    [HttpPut("{id}")]
    public async Task<IResult> Put(int id, RetailBranch input, TodoContext db){
        if (id != input.BranchId){
            return Results.BadRequest();
        }
        db.Entry(input).State = EntityState.Modified;

        try {
            await db.SaveChangesAsync();
        }

        catch(DbUpdateConcurrencyException){
            return Results.NotFound();
        }
        return Results.NoContent();
    }

    // app.MapDelete("/todoitems/{id}", async (int id, TodoContext db) => {
    //     if (await db.RetailBranches.FindAsync(id) is RetailBranch todo){
    //         db.RetailBranches.Remove(todo);
    //         await db.SaveChangesAsync();
    //         return Results.Ok(todo);
    //     }
    //     return Results.NotFound();
    // });

    [HttpDelete("{id}")]
    public async Task<IActionResult> Delete(int id, TodoContext db){

        if (await db.RetailBranches.FindAsync(id) is RetailBranch todo){
            db.RetailBranches.Remove(todo);
            await db.SaveChangesAsync();
            return NoContent();
        }
        return NotFound();
    }
}