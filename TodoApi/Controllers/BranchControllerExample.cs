// using System;
// using System.Collections.Generic;
// using System.Linq;
// using System.Threading.Tasks;
// using Microsoft.AspNetCore.Http;
// using Microsoft.AspNetCore.Mvc;
// using Microsoft.EntityFrameworkCore;
// using TodoApi.Models;

// namespace TodoApi.Controllers
// {
//     [Route("api/[controller]")]
//     [ApiController]
//     public class BranchController : ControllerBase
//     {
//         private readonly TodoContext _context;

//         public BranchController(TodoContext context)
//         {
//             _context = context;
//         }

//         // GET: api/Branch
//         [HttpGet]
//         public async Task<ActionResult<IEnumerable<RetailBranch>>> GetRetailItems()
//         {
//           if (_context.RetailBranches == null)
//           {
//               return NotFound();
//           }
//             return await _context.RetailBranches.ToListAsync();
//         }

//         // GET: api/Branch/5
//         [HttpGet("{id}")]
//         public async Task<ActionResult<RetailBranch>> GetRetailItem(int id)
//         {
//           if (_context.RetailBranches == null)
//           {
//               return NotFound();
//           }
//             var retailItem = await _context.RetailBranches.FindAsync(id);

//             if (retailItem == null)
//             {
//                 return NotFound();
//             }

//             return retailItem;
//         }

//         // PUT: api/Branch/5
//         // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
//         [HttpPut("{id}")]
//         public async Task<IActionResult> PutRetailItem(int id, RetailBranch retailItem)
//         {
//             if (id != retailItem.BranchId)
//             {
//                 return BadRequest();
//             }

//             _context.Entry(retailItem).State = EntityState.Modified;

//             try
//             {
//                 await _context.SaveChangesAsync();
//             }
//             catch (DbUpdateConcurrencyException)
//             {
//                 if (!RetailItemExists(id))
//                 {
//                     return NotFound();
//                 }
//                 else
//                 {
//                     throw;
//                 }
//             }

//             return NoContent();
//         }

//         // POST: api/Branch
//         // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
//         [HttpPost]
//         public async Task<ActionResult<RetailBranch>> PostRetailItem(RetailBranch retailItem)
//         {
//           if (_context.RetailBranches == null)
//           {
//               return Problem("Entity set 'TodoContext.RetailBranches'  is null.");
//           }
//             _context.RetailBranches.Add(retailItem);
//             await _context.SaveChangesAsync();

//             return CreatedAtAction("GetRetailItem", new { id = retailItem.BranchId }, retailItem);
//         }

//         // DELETE: api/Branch/5
//         [HttpDelete("{id}")]
//         public async Task<IActionResult> DeleteRetailItem(int id)
//         {
//             if (_context.RetailBranches == null)
//             {
//                 return NotFound();
//             }
//             var retailItem = await _context.RetailBranches.FindAsync(id);
//             if (retailItem == null)
//             {
//                 return NotFound();
//             }

//             _context.RetailBranches.Remove(retailItem);
//             await _context.SaveChangesAsync();

//             return NoContent();
//         }

//         private bool RetailItemExists(int id)
//         {
//             return (_context.RetailBranches?.Any(e => e.BranchId == id)).GetValueOrDefault();
//         }
//     }
// }
