using Microsoft.EntityFrameworkCore;
using TodoApi.Models;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddDbContext<TodoContext>(options =>
    options.UseInMemoryDatabase("TodoList"));
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
var app = builder.Build();

//Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

//app.UseAuthorization();

app.MapControllers();

// app.MapGet("/todoitems", async (TodoContext db) =>
//     await db.RetailBranches.ToListAsync()
// );

// app.MapPost("/todoitems", async (RetailBranch todo, TodoContext db) =>
//     {
//         db.RetailBranches.Add(todo);
//         await db.SaveChangesAsync();
//         return Results.Created($"/insert directory of db/{todo.BranchId}", todo);
//     }
// );

// app.MapPut("/todoitems/{id}", async (int id, RetailBranch input, TodoContext db) => {
//     var todo = await db.RetailBranches.FindAsync(id);

//     if (todo is null) return Results.NotFound();

//     todo.Name = input.Name;
//     // set state of item in database
//     await db.SaveChangesAsync();

//     return Results.NoContent();
// });

// app.MapDelete("/todoitems/{id}", async (int id, TodoContext db) => {
//     if (await db.RetailBranches.FindAsync(id) is RetailBranch todo){
//         db.RetailBranches.Remove(todo);
//         await db.SaveChangesAsync();
//         return Results.Ok(todo);
//     }
//     return Results.NotFound();
// });

app.Run();
